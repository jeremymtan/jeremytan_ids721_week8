/*

To run:

cargo run --  --message "Off to the bunker. Every person for themselves" --encrypt

To decrypt:

cargo run --  --message "{base64 input}" --decrypt

*/
#![allow(deprecated)]
use clap::Parser;
use jeremytan_ids721_week8::{decrypt, encrypt};
use rustc_serialize::base64::{FromBase64, ToBase64, STANDARD};
use std::process::Command;

// The key to use for cipher
static KEY: [u8; 32] = [
    11, 240, 187, 101, 86, 31, 106, 213, 158, 232, 52, 243, 245, 149, 250, 129, 249, 152, 173, 1,
    82, 169, 242, 238, 28, 88, 67, 186, 127, 88, 129, 245,
];

// The IV to use for cipher
static IV: [u8; 16] = [
    171, 250, 33, 152, 5, 58, 106, 206, 82, 13, 91, 144, 70, 67, 203, 91,
];

/// CLI tool to encrypt and decrypt messages using the caeser cipher
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Encrypt the message
    #[arg(short, long)]
    encrypt: bool,

    /// decrypt the message
    #[arg(short, long)]
    decrypt: bool,

    /// The message to encrypt or decrypt
    #[arg(short, long)]
    message: String,

    /// The key to use for cipher
    #[arg(
        short,
        long,
        default_value = "11, 240, 187, 101, 86, 31, 106, 213, 158, 232, 52, 243, 245, 149, 250, 129, 249, 152, 173, 1, 82, 169, 242, 238, 28, 88, 67, 186, 127, 88, 129, 245"
    )]
    key: String,

    /// The iv to use for cipher
    #[arg(
        short,
        long,
        default_value = "171, 250, 33, 152, 5, 58, 106, 206, 82, 13, 91, 144, 70, 67, 203, 91"
    )]
    iv: String,
}
// run it
fn main() {
    let args = Args::parse();

    let key: [u8; 32] = (&args.key)
        .split(',')
        .map(|s| s.trim().parse().expect(s))
        .collect::<Vec<u8>>()
        .try_into()
        .expect("Expected a string with exactly 32 u8 values separated by commas");
    let iv: [u8; 16] = (&args.iv)
        .split(',')
        .map(|s| s.trim().parse().expect("Failed to parse u8"))
        .collect::<Vec<u8>>()
        .try_into()
        .expect("Expected a string with exactly 16 u8 values separated by commas");
    println!("key {}", key.to_base64(STANDARD));
    println!("iv {}", iv.to_base64(STANDARD));

    if args.encrypt {
        let encoded = encrypt((&args.message).as_bytes(), &key, &iv).ok().unwrap();
        println!("{}", encoded.to_base64(STANDARD));
    } else if args.decrypt {
        let decoded_bytes = match (&args.message).from_base64() {
            Ok(decoded_bytes) => decoded_bytes,
            Err(err) => {
                println!("Error decoding Base64: {:?}", err);
                return; // Exit or handle the error as needed.
            }
        };
        let decoded = decrypt(&decoded_bytes, &key, &iv).ok().unwrap();
        let decoded_string: String = decoded.iter().map(|&d| d as u8 as char).collect();
        println!("{}", decoded_string);
    } else {
        println!("Please specify either --encrypt or --decrypt");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encrypt_decrypt() {
        let original_data = b"Hello, World!";

        let encrypted_data = encrypt(original_data, &KEY, &IV).expect("Encryption failed");
        let decrypted_data = decrypt(&encrypted_data, &KEY, &IV).expect("Decryption failed");

        // Convert the data to strings for better error messages in case of failure
        let original_str = String::from_utf8_lossy(original_data);
        let decrypted_str = String::from_utf8_lossy(&decrypted_data);

        assert_eq!(original_data, &decrypted_data[..]);
        println!("Original: {}", original_str);
        println!("Decrypted: {}", decrypted_str);
    }
}
