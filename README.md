[![pipeline status](https://gitlab.com/jeremymtan/jeremytan_ids721_week8/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/jeremymtan/jeremytan_ids721_week8/-/commits/main)
# AES-256 CBC PKCS#7 ClI tool 
The project is a command-line interface (CLI) tool implemented in Rust for encrypting and decrypting messages using the AES-256 CBC PKCS#7 encryption algorithm. It supports two modes: encryption and decryption. The Rust cli uses the aes_cbc crate for encryption and decryption. It employs the clap library for parsing command-line arguments. The CLI takes input for mode (encrypt or decrypt), the message to be encrypted or decrypted, and optional arguments for the encryption key and initialization vector (IV).

## Adressing skipped pipleline 
There is a current issue with gitlab piplelines that can be found here: just refrence this issue for skipped badge :
https://gitlab.com/gitlab-org/gitlab/-/issues/326513

An individual has just recently started to fix this. Essentialy, to have the pipeline generate the report on the fly I require the job to "push back" to my pipeline. To not have an infinite loop, I add `ci.skip` to stop the pipeline from beign created again from the job bot (that pushes the report to the pipeline). You can see my successful pipeline and it skipping the pipeline here:

![Screenshot_2024-04-08_at_7.19.01_PM](/uploads/b4e205052ce6c90e058b6b54b228d4bd/Screenshot_2024-04-08_at_7.19.01_PM.png)

## Using the CLI tool
1. running `cargo run --  --message "Off to the bunker. Every person for themselves" --encrypt` or using your own string will output the encryped message
2. running `cargo run --  --message "Off to the bunker. Every person for themselves" --decrypt` or using your own string will output the decrypted message (of course if you just copy the above it will be nonsensical as the message is not encrypted)

## Preparation 
1. Install rust and copy my `cargot.toml` dependencies 
2. Copy my `lib.rs` and `main.rs` to grab the commandline interface and libarary fucntions for encryption and decryption 
3. build: `cargo build` for dependencies installation
4. run: `cargo run --  --message "Off to the bunker. Every person for themselves" --encrypt` or use your own string
5. Format code `make format`
6. Lint code `make lint`
7. Test coce `make test`

## References 
1. [rust-cli-template](https://github.com/kbknapp/rust-cli-template)
2. https://github.com/DaGenix/rust-crypto/
3. https://github.com/nogibjj/rust-data-engineering
4. https://gitlab.com/gitlab-org/gitlab/-/issues/326513
