rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

lint:
	cargo clippy --quiet

build:
	cargo build --release

test:
	cargo test -- --show-output >> test.md

run:
	cargo run --  --message "Off to the bunker. Every person for themselves" --encrypt

release:
	cargo build --release

all: format lint test run
